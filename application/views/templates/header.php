<html>
<head>
<title><?php echo $title; ?></title>
<link rel="stylesheet" type="text/css" href="<? echo base_url();?>/css/style.css">
</head>

<body>
<div class="topbar">    
    <h1>Cinema - <?php echo $title; ?></h1>
</div>
<div class="sidebar">
    <a href="<?php echo base_url();?>"><div class="button"> HOME </div></a>
    <a href="<?php echo base_url();?>index.php/user_authentication/index"><div class="button">LOGIN</div></a>
    <a href="<?php echo base_url();?>index.php/profile/index"><div class="button">PROFILE</div></a>
    <a href="<?php echo base_url();?>index.php/pages/view/search"><div class="button">SEARCH</div></a>
    <a href="<?php echo base_url();?>index.php/upload/index"><div class="button">UPLOAD</div></a> 
    <a href="<?php echo base_url();?>index.php/lists/index"><div class="button">LISTS</div></a> 
    <a href="<?php echo base_url();?>index.php/user_authentication/logout"><div class="button">LOGOUT</div></a>
</div>