<div class="main">

<?php

$options_drop = array(
    '1'         => '1',
    '2'           => '2',
    '3'         => '3',
    '4'        => '4',
    '5'        => '5',
);
$hidden = array('movie_title' => $movie_title);
echo form_open('review/write','', $hidden);
echo form_label('Rating : ');
echo"<br/>";
echo form_dropdown('rating', $options_drop);
echo"<br/>";
echo"<br/>";


$options_text = array(
    'name' => 'text',
    'value' => 'text',
    'rows'  => '10',
    'cols'  => '80'
);
echo form_label('Write your review : ');
echo"<br/>";
echo form_textarea($options_text);
echo"<br/>";
echo"<br/>";
echo form_submit('submit', 'Submit');
echo form_close();
?>
</div>