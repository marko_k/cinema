<div class="main">
    <div class="item">
    <b><?php echo $item->title; ?></b>
    <br>
    director: <?php echo $item->name; ?>
    <br>
    <?php echo $item->year; ?>
    <br>
    <?php echo $item->summary; ?>
    <br>
    genre: <?php echo $item->genre; ?>
    <br>
    budget: <?php echo $item->budget; ?>$
    <br>
    rating: <?php echo $item->rating_average; ?>
    <br>
    <a style="text-align:left; float: left;" href="<?php echo base_url()?>index.php/review/index?movie_title=<?php echo $item->title; ?>">Write a review.</a>
    <a style="text-align:right;float: right;" href="<?php echo base_url()?>index.php/review/show?movie_title=<?php echo $item->title; ?>">Show reviews.</a>
    <br><br>
    <a style="text-align:center; float: center;" href="<?php echo base_url()?>index.php/lists/add_movie_form?movie_id=<?php echo $item->movie_id; ?>">Add to list</a>
    </div>
</div>