<?php 
class List_model extends CI_Model{ 
        public function __construct(){
        $this->load->database();
    }


    public function get_user_lists($user_id) {
        $condition = "author =" . "" . $user_id . "";
        $this->db->select('*');
        $this->db->from('List');
        $this->db->where("author = 3");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_movies($list_id) {
        $this->db->select('Film.title');
        $this->db->from('Film');
        $this->db->where("List.list_id = " . $list_id);
        $this->db->join('Film_list', 'Film.movie_id = Film_list.film_id', 'left');
        $this->db->join('List', 'List.list_id = Film_list.list_id', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    public function make_list($user_id, $list_name) {
        //insert into list (name, author) VALUES ($listname, $user_id)

        $insertdata = array(
            'name' => $list_name,
            'author' => $user_id
        );
        $this->db->insert('List', $insertdata);
    }

    public function add_movie($user_id, $list_id, $movie_id) {
        $insertdata = array(
            'film_id' => $movie_id,
            'list_id' => $list_id
        );
        $this->db->insert('Film_list', $insertdata);

    }

}