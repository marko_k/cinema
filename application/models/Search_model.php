<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_Model extends CI_Model {
    public function __construct(){
        //$this->load->database();
    }
    public function GetSearchdata($search_parameter = ''){
            $this->db->select('Film.movie_id, Film.title, Person.name, Film.year, Film.summary, Film.genre, Film.budget, Film.rating_average');
            $this->db->from('Film');
            $this->db->join('Person','Film.director=Person.person_id','right.outer');
            $this->db->like('title', $search_parameter);
            $this->db->or_like('name', $search_parameter);
            $this->db->or_like('year', $search_parameter);
            $query = $this->db->get();
            return $query;
    }
    

    public function GetReviews($movie_name){
            $this->db->select('rating, date, text, User.username, Film.title');
            $this->db->from('Review');
            $this->db->join('User','Review.author=User.user_id','right.outer');
            $this->db->join('Film','Review.movie=Film.movie_id','right.outer');
            $this->db->where('title', $movie_name);
            $query = $this->db->get();
            return $query;
    }

    public function CreateMovie($upload_data){
        $this->db->select('person_id');
        $this->db->from('Person');
        $this->db->where('name', $upload_data['director']);
        $query = $this->db->get();
        $per_id = $query->row();

        //if director not in database add them 
        if ($per_id == NULL){
            $insert_director = array('name' => $upload_data['director']);
            $this->db->insert('Person', $insert_director);
            $this->db->select('person_id');
            $this->db->from('Person');
            $this->db->where('name', $upload_data['director']);
            $query = $this->db->get();
            $per_id = $query->row();
        }


        $insert_data = array(
            'title' => $upload_data['title'], 
            'director' => $per_id->person_id, 
            'year' => $upload_data['year'],
            'summary' =>$upload_data['summary'],
            'genre' => $upload_data['genre'],
            'budget' => $upload_data['budget'],
        );
        $this->db->insert('Film', $insert_data);
    }






    public function CreateReview($reviewdata){
        // $author_i = $this->db->select('user_id')->from('User')
        // ->where('username', $reviewdata['username']);


         $this->db->select('user_id');
         $this->db->from('User');
         $this->db->where('username', $reviewdata['username']);
         $query = $this->db->get();
         
         $author_i = $query->row();
 
         $this->db->select('movie_id');
         $this->db->from('Film');
         $this->db->where('title', $reviewdata['movie_title']);
         $query = $this->db->get();
         
         $movie_i = $query->row();
 
        // $movie_i = $this->db->select('movie_id')->from('Film')
         //->where('title', $reviewdata['movie_title']);
 
         $insert_data = array(
             'rating' => $reviewdata['rating'],
             'text' => $reviewdata['text'],
             'author'=> $author_i->user_id,
             'movie'=> $movie_i->movie_id
         );
 
         $this->db->insert('Review', $insert_data);
         
     }
 
    
}