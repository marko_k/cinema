<?php 
class Login_database extends CI_Model{ 
        public function __construct(){
        $this->load->database();
    }
    // Insert registration data in database
    public function registration_insert($data) {
        // Query to check whether username already exist or not
        $condition = "username =" . "'" . $data['username'] . "'";
        $this->db->select('*');
        $this->db->from('User');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {

            // Query to insert data in database
            $this->db->insert('User', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }
        } else {
            return false;
        }
    }

    public function profile_update($data) {
        if ($data['username'] != '') {
            $this->db->set('username', $data['username']);
            $this->db->where('username', $data['old_username']);
            $this->db->update('User');
            $this->session->set_userdata('username',$data['username']);
        } 

        if ($data['password'] != '') {
            $this->db->set('password', $data['password']);
            $this->db->where('username', $this->session->userdata('username'));
            $this->db->update('User');
        } 
        if ($data['name'] != '') {
            $this->db->set('name', $data['name']);
            $this->db->where('username', $this->session->userdata('username'));
            $this->db->update('User');
        } 
        if ($data['surname'] != '') {
            $this->db->set('surname', $data['surname']);
            $this->db->where('username', $this->session->userdata('username'));
            $this->db->update('User');
        } 
        return TRUE;
    }

    // Read data using username and password
    public function login($data) {

        $condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password'] . "'";
        $this->db->select('*');
        $this->db->from('User');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    // Read data from database to show data in admin page
    public function read_user_information($username) {

        $condition = "username =" . "'" . $username . "'";
        $this->db->select('*');
        $this->db->from('User');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

}
