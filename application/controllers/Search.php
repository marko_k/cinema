<?php 
class Search extends CI_Controller{
    public function __construct() {
    parent::__construct();

    $this->load->helper('url_helper');

    // Load form helper library
    $this->load->helper('form');

    // Load form validation library
    $this->load->library('form_validation');

    // Load session library
    $this->load->library('session');

    // Load model
    $this->load->model('Search_model');    
    }

    
  public function search($page = 'search'){
      $parameter = $this->input->get('search', TRUE);
      $data['searchdata']=$this->Search_model->GetSearchdata($parameter);
      $data['title'] = ucfirst($page);

      $this->load->view('templates/header', $data);
      $this->load->view('pages/search',$data);

      foreach ($data['searchdata']->result() as $row){
        $data['item'] = $row;
        $this->load->view('item.php', $data);
      }
      $this->load->view('templates/footer', $data); 
  }
}
