<?php 
class Review extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Search_model');    
    }

        
    public function index($page = 'reviews'){
        $movie_title=$this->input->get('movie_title');
        $data['title'] = ucfirst($page);
        $data['movie_title'] = $movie_title;
        if($this->session->userdata('logged_in')){
            $this->load->view('templates/header', $data);
            $this->load->view('review_form', $data);
            $this->load->view('templates/footer', $data); 
        }
        else{
            $this->load->view('templates/header', $data);
            $this->load->view('pages/loginmsg', $data);
            $this->load->view('templates/footer', $data); 
        }
    }


    public function show($page = 'reviews'){
        $data['title'] = urldecode(ucfirst($page));
  
        $this->load->view('templates/header', $data);
        $movie_title=$this->input->get('movie_title');
        $reviewquery = $this->Search_model->GetReviews($movie_title);
        $data['reviews'] = $reviewquery;
        foreach ($data['reviews']->result() as $row){
          $data['review'] = $row;
          $this->load->view('pages/review.php', $data);
        }
        $this->load->view('templates/footer', $data); 
    }

    public function write($page = 'reviews'){
        $data['title'] = ucfirst($page);
        $text=$this->input->post('text');
        $rating=$this->input->post('rating');
        $movie_title=$this->input->post('movie_title');
        $reviewdata=array(
            'text' => $text, 
            'rating'=> $rating,
            'movie_title' => $movie_title,
            'username' => $this->session->userdata('username')
        );

        //$this->load->view('pages/error', $reviewdata);
        $this->Search_model->CreateReview($reviewdata);
        $this->load->view('templates/header', $data);
        $this->load->view('pages/review_success.php', $data);
        $this->load->view('templates/footer', $data);

    }
}