<?php 
class Profile extends CI_Controller{
    public function __construct() {
        parent::__construct();   
        $this->load->helper('form'); 
        $this->load->library('session');
        $this->load->model('Profile_model');

    }

    public function index($page = 'profile') {
        $data['title'] = ucfirst($page);
        if(!$this->session->userdata('logged_in')){
            $this->load->view('templates/header', $data);
            $this->load->view('pages/loginmsg', $data);
            $this->load->view('templates/footer', $data);
        }
        else{
            $queryres = $this->Profile_model->read_user_information($this->session->username);
            $data['username'] = $queryres[0]->username;
            $data['name'] = $queryres[0]->name;
            $data['surname'] = $queryres[0]->surname;
            $data['type'] = $queryres[0]->type;
            $data['picture'] = $queryres[0]->picture;
            $data['title'] = ucfirst($page);
            $this->load->view('templates/header', $data);
            $this->load->view('pages/profile_page', $data);
            $this->load->view('profile_form', $data);
            $this->load->view('pages/image_form', $data);
            $this->load->view('templates/footer', $data);
        }
    }
}