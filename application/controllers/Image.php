<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Image extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        
    }

    public function index() {
    }

    public function do_upload() {
        $config['upload_path'] = './application/images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 2000;
        $config['max_width'] = 1500;
        $config['max_height'] = 1500;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('pages/error', $error);
        } else {
            $data = array('upload_data' => $this->upload->data());
            $filename = $this->upload->data('file_name');
            
            
            $this->db->set('picture', $filename);
            $this->db->where('username', $this->session->userdata('username'));
            $this->db->update('User');

            
            $page = 'home';
            $data['title'] = ucfirst($page);
            $this->load->view('templates/header', $data);
            $this->load->view('pages/success', $data);
            $this->load->view('templates/footer', $data);
        }
    }
}
?>