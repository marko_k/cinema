<?php 
class User_authentication extends CI_Controller{

    public function __construct() {
    parent::__construct();

    $this->load->helper('url_helper');

    // Load form helper library
    $this->load->helper('form');

    // Load form validation library
    $this->load->library('form_validation');

    // Load session library
    $this->load->library('session');

    // Load database
    $this->load->model('login_database');

    $this->load->model('Profile_model');
        
    }

    // Show login page
    public function index($page = 'login') {
        $data['title'] = ucfirst($page);
        $this->load->view('templates/header', $data);
        $this->load->view('login_form', $data);
        $this->load->view('templates/footer', $data);
    }

    // Show registration page
    public function show($page = 'register') {
        $data['title'] = ucfirst($page);
        $this->load->view('templates/header', $data);
        $this->load->view('registration_form', $data);
        $this->load->view('templates/footer', $data);
    }

    // Validate and store registration data in database
    public function signup($page = 'register') {
        $data['title'] = ucfirst($page);
        // Check validation for user input in SignUp form
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['title'] = ucfirst($page);
            $this->load->view('templates/header', $data);
            $this->load->view('registration_form');
            $this->load->view('templates/footer', $data);
        } else {
            $data = array(
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password')
            );
            $result = $this->login_database->registration_insert($data);
            if ($result == TRUE) {
                $data['title'] = ucfirst($page);
                $this->load->view('templates/header', $data);
                $data['message_display'] = 'Registration Successfully !';
                $this->load->view('login_form', $data);
                $this->load->view('templates/footer', $data);
            } else {
                $data['title'] = ucfirst($page);
                $this->load->view('templates/header', $data);
                $this->load->view('registration_form');
                $this->load->view('templates/footer', $data);
            }
        }
    }


    //update profile info and store in database
    public function update($page = 'profile') {
        $data['title'] = ucfirst($page);
            $data = array(
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
                'name' => $this->input->post('name'),
                'surname' => $this->input->post('surname'),
                'old_username' => $this->session->userdata('username')
            );
            $result = $this->login_database->profile_update($data);
            if ($result == TRUE) {
                $data['title'] = ucfirst($page);
                $this->load->view('templates/header', $data);
                $data['message_display'] = 'succesfully updated!';
                $this->load->view('pages/success', $data);
                $this->load->view('templates/footer', $data);
            } else {
                $data['message_display'] = 'error in update function';
                $this->load->view('pages/error', $data); 
            }
        
    }


    // Check for user login process
    public function signin($page = 'login') {
        $data['title'] = ucfirst($page);

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        $data = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password')
        );
        $result = $this->login_database->login($data);
        if ($result == TRUE) {

            $username = $this->input->post('username');
            $result = $this->login_database->read_user_information($username);
            if ($result != false) {
                $session_data = array(
                    'username' => $result[0]->username,
                    'logged_in' => TRUE,
                    'user_id' => $result[0]->user_id
                );
                // Add user data in session
                $data = array('error_message' => 'Signin OK');
                $data['title'] = ucfirst($page);
                $this->session->set_userdata($session_data);
                $this->load->view('templates/header', $data);
                $this->load->view('login_form',$data);
                $this->load->view('templates/footer', $data);
            }
        } else {
            $data = array(
                'error_message' => 'Invalid Username or Password'
            );
            $data['title'] = ucfirst($page);
            $this->load->view('templates/header', $data);
            $this->load->view('login_form', $data);
            $this->load->view('templates/footer', $data);
        }
    }

    // Logout from admin page
    public function logout($page = 'login') {
        $data['title'] = ucfirst($page);

        // Removing session data
        $sess_array = array(
            'username' => ''
        );
        $data['title'] = ucfirst($page);
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('username');
        $this->load->view('templates/header', $data);
        $data['message_display'] = 'Successfully Logout';

        $this->load->view('login_form', $data);
    }



}
