<?php 
class Lists extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->helper('url_helper');

        $this->load->helper('form');

        $this->load->library('form_validation');

        $this->load->library('session');

        $this->load->model('list_model');

        $this->load->library('session');


    }

    public function index($page = 'lists') {
        $data['title'] = ucfirst($page);
        if(!$this->session->userdata('logged_in')){  //if not logged in
            $this->load->view('templates/header', $data);
            $this->load->view('pages/loginmsg', $data);
            $this->load->view('templates/footer', $data);        
        }
        else {

            $query_result=$this->list_model->get_user_lists($this->session->user_id);
            $data['list_data'] = $query_result;
            

            $this->load->view('templates/header', $data);
            foreach ($data['list_data'] as $row){
                $data['movie_list'] = $this->list_model->get_movies($row->list_id);
                $data['list_result'] = $row;
                $this->load->view('list_item', $data);
            }

            $this->load->view('templates/footer', $data);  
        }
    }

    public function create_list($page = 'lists') {

    }

    public function add_movie_form() {
        $page='lists';
        $data['title'] = ucfirst($page);
        if(!$this->session->userdata('logged_in')){  //if not logged in
            $this->load->view('templates/header', $data);
            $this->load->view('pages/loginmsg', $data);
            $this->load->view('templates/footer', $data);        
        }
        else {
            $page = 'lists';
            $selected_list='';
            $data['title'] = $page;
            $data['movie_id'] = $this->input->get('movie_id');
            $data['list_dropdown'] = $this->list_model->get_user_lists($this->session->user_id);
            $this->load->view('templates/header', $data);
            $this->load->view('list_form', $data);
            $this->load->view('templates/footer', $data);
        }
    }

    public function add_movie() {
        $page = 'lists';
        $data['title'] = $page;
        $user_id = $this->session->user_id;
        $list_id = $this->input->get('list_id');
        $movie_id = $this->input->get('movie_id');
        $this->list_model->add_movie($user_id, $list_id, $movie_id);

        $this->load->view('templates/header', $data);
        $this->load->view('pages/listmsg', $data);
        $this->load->view('templates/footer', $data);  
    }


}
