<?php 
class Upload extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Search_model');    
    }

        
    public function index($page = 'upload'){
        $data['title'] = ucfirst($page);
        if($this->session->userdata('logged_in')){
            $this->load->view('templates/header', $data);
            $this->load->view('upload_form', $data);
            $this->load->view('templates/footer', $data); 
        }
        else{
            $this->load->view('templates/header', $data);
            $this->load->view('pages/loginmsg', $data);
            $this->load->view('templates/footer', $data); 
        }
    }


    public function write($page = 'reviews'){
        $data['title'] = ucfirst($page);

        $title = $this->input->post('title');
        $director = $this->input->post('director');
        $year = $this->input->post('year');
        $summary = $this->input->post('summary');
        $genre = $this->input->post('genre');
        $budget = $this->input->post('budget');
        
        $upload_data = array(
            'title' => $title, 
            'director' => $director, 
            'year' => $year, 
            'summary' => $summary ,
            'genre' => $genre ,
            'budget' => $budget 
        );

        //$this->load->view('pages/error', $upload_data);
        $this->Search_model->CreateMovie($upload_data);
        $this->load->view('templates/header', $data);
        $this->load->view('pages/upload_success.php', $data);
        $this->load->view('templates/footer', $data);
    }
}